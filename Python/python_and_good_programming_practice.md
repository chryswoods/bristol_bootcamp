
# Python and Good Programming Practice

Welcome to a series of sessions that will aim to teach you the tools of good programming practice and 
software design. We will be using Python for these sessions, but the ideas and techniques and very
similar for all programming languages.

Hopefully you are all familiar with the basics of Python. If not, you can look at 
this [Beginner's Python](http://chryswoods.com/beginning_python) course, or you can ask
us as we come around the room.

## Timetable

The course is divided into a related series of sessions:

This morning

 * [Python - Containers, Lists and Dictionaries](1_lists_and_dictionaries.md)

This afternoon

 * [Good Programming Practice - Functions and Modules](2_functions_and_modules.md)
 * [Good Programming Practice - Documenting Code](3_documenting_code.md)

Tomorrow

 * [Object Orientation](4_object_orientation.md)
 * [Program Design as Exemplified in NumPy](5_numpy.md)


